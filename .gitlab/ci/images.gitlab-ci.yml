include:
  # debian based images
  - local: .gitlab/ci/debian.gitlab-ci.yml
    rules:
      - if: '$UBI_PIPELINE != "true" && $CI_COMMIT_TAG !~ /^v\d+\.\d+\.\d+(-rc\d+)?-ubi8$/ && $CI_COMMIT_REF_NAME !~ /-ubi8$/ && $FIPS_PIPELINE != "true" && $CI_COMMIT_TAG !~ /^v\d+\.\d+\.\d+(-rc\d+)?-fips$/ && $CI_COMMIT_REF_NAME !~ /-fips$/'
  # ubi based images
  - local: .gitlab/ci/ubi.gitlab-ci.yml
    rules:
      - if: '$UBI_PIPELINE == "true" || $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-rc\d+)?-ubi8$/ || $CI_COMMIT_REF_NAME =~ /-ubi8$/'
  # fips based images
  - local: .gitlab/ci/fips.gitlab-ci.yml
    rules:
      - if: '$FIPS_PIPELINE == "true" || $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-rc\d+)?-fips$/ || $CI_COMMIT_REF_NAME =~ /-fips$/'

# Common jobs for image pipelines

trigger-chart-test:
  stage: final-list
  trigger:
    project: 'gitlab-org/charts/gitlab'
    branch: master-trigger-branch
  inherit:
    variables: false
  variables:
    GITLAB_VERSION: "${CI_COMMIT_REF_SLUG}"
    REVIEW_REF_PREFIX: "${CI_PIPELINE_ID}-"
  rules:
    # Run only on main .com repo branches
    - if: '$CI_COMMIT_BRANCH && $CI_PROJECT_PATH == "gitlab-org/build/CNG"'
      when: manual
      allow_failure: true
    - when: never

final-images-listing:
  extends: .job-base
  stage: final-list
  script:
    - for i in artifacts/final/* ; do echo "${i} - $(cat ${i})" ; done
  needs: !reference [.final_images]
  rules:
    # Skipped for auto-deploy branches via workflow:rules
    # Skip on deps pipeline as the final image listing is useless there
    - !reference [.except-deps, rules]
  artifacts:
    paths:
      - artifacts/final/

image-metrics:
  # intentionally not using .job-base
  stage: final-list
  image: ${DEPENDENCY_PROXY}${ALPINE_IMAGE}
  script:
    - apk add skopeo jq
    - . build-scripts/metrics.sh
    - generate_image_metrics
  needs: !reference [.final_images]
  rules: # NOTE: never runs for auto-deploy branches. See `workflow:rules` above.
    - !reference [.tagless-versionless, rules]
      # Run on any Branch pipeline on .com (where registry is public)
      # CI_COMMIT_BRANCH is available in branch pipelines. Not available in MR (detached) or Tag pipelines.
    - if: '$CI_COMMIT_BRANCH && $CI_PROJECT_PATH == "gitlab-org/build/CNG"'
  artifacts:
    paths:
      - artifacts/final/
    reports:
      metrics: metrics/image_metrics.txt

sync-images:
  image: "${DEPENDENCY_PROXY}docker:git"
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
  services:
    - docker:${DOCKER_VERSION}-dind
  stage: release
  allow_failure: false
  script:
    - cat artifacts/images/* > image_versions.txt
    - rm -rf artifacts/*
    - sh build-scripts/docker_image_sync.sh image_versions.txt
    - if $(echo $CI_COMMIT_TAG | grep -Eq 'v*-ubi8'); then
    -   apk add curl
    -   UBI_IMAGES=$(cat artifacts/cng_images.txt | xargs printf ', %s')
    -   curl -fS
          --request POST
          --form ref=master
          --form "variables[IMAGES]=${UBI_IMAGES:2}"
          --form "token=${SCANNING_TRIGGER_TOKEN}"
          "${SCANNING_TRIGGER_PIPELINE}"
    - fi
  artifacts:
    paths:
      - artifacts/
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PROJECT_PATH == "gitlab/charts/components/images"'
      when: manual

container-scanning:
  stage: container-scanning
  needs: ["final-images-listing"]
  allow_failure: true
  script:
    - images=$(cat artifacts/final/* | xargs printf "$CI_REGISTRY_IMAGE/%s,")
    - curl -fS
        --request POST
        --form ref=master
        --form "variables[IMAGES]=$images"
        --form "token=${SCANNING_TRIGGER_TOKEN}"
        https://gitlab.com/api/v4/projects/16505542/trigger/pipeline
  rules:
    # run on on Dev for CI_DEFAULT_BRANCH
    - if: '$CI_PROJECT_PATH == "gitlab/charts/components/images" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # run for FIPS tag pipelines and scheduled FIPS pipelines
    - !reference [.if_fips_tag]
    - !reference [.if_scheduled_fips_pipeline]
    # allowed to be run manually other FIPS pipelines. Allowing failure so the
    # job is non-blocking. Scheduled FIPS pipeline will be caught by above
    # rule. We can't use !reference tag here because we want to change when and
    # allow_failure
    - if: '$CI_COMMIT_REF_NAME =~ /-fips$/ || $FIPS_PIPELINE == "true" '
      when: manual
      allow_failure: true
